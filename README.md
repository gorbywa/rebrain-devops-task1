# Template Repository
> Limited set of markdown examples v.1.0.0.1

## :bulb: Some text editors with markdown support

#### Free of charge
1. [Ghostwriter](https://wereturtle.github.io/ghostwriter/)
2. [Typora](https://typora.io/)
3. [Atom](https://atom.io/)

#### With paid license
4. [Caret](https://caret.io/)
5. [Ulysses](https://ulysses.app/)

## :bulb: Text formating
- *Italic text*
- **Bold text**
- <s>Strikethrough</s>
-<s>_**Can be striked bold and italic**_</s>


```
Text might be highlighted this way
```
## :bulb: Lists
+ Some Text
 + Also some Text
+It's OK to be bored

## :bulb: Tables
Header 1 | Header 2
-------- | --------
First Cell| Second Cell
...|...

## :bulb: Check boxes
- [x] Checked
 - [ ] Unchecked
- [x] Checked again

## :bulb: Code examples
```javascript
if (I_Dont_Code_Yet) {
  return True
}
```
***
## Sourses
:+1: [@armakuni](https://gist.github.com/PurpleBooth)  
:ok_hand:  [paulradzkov_md_cheatsheet](https://paulradzkov.com/2014/markdown_cheatsheet/)  
:floppy_disk: [pandao_editor.md](https://pandao.github.io/editor.md/en.html)  

# The end
